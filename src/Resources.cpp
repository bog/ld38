#include <Resources.hpp>
#include <cassert>

Resources::Resources()
{
}

void Resources::load_font(std::string name, std::string path)
{
  auto itr = m_fonts.find(name);
  assert( itr == m_fonts.end() );

  sf::Font* font = new sf::Font();
  font->loadFromFile(path);

  m_fonts.insert(std::make_pair(name, std::unique_ptr<sf::Font>(font)));
}

sf::Font& Resources::font(std::string name)
{
  auto itr = m_fonts.find(name);
  assert( itr != m_fonts.end() );
  return *m_fonts[name].get();
}

void Resources::load_texture(std::string name, std::string path)
{
  auto itr = m_textures.find(name);
  assert( itr == m_textures.end() );

  sf::Texture* texture = new sf::Texture();
  texture->loadFromFile(path);

  m_textures.insert(std::make_pair(name, std::unique_ptr<sf::Texture>(texture)));
}

sf::Texture& Resources::texture(std::string name)
{
  auto itr = m_textures.find(name);
  assert( itr != m_textures.end() );
  return *m_textures[name].get();
}

void Resources::load_soundbuffer(std::string name, std::string path)
{
  auto itr = m_soundbuffers.find(name);
  assert( itr == m_soundbuffers.end() );

  sf::SoundBuffer* soundbuffer = new sf::SoundBuffer();
  soundbuffer->loadFromFile(path);

  m_soundbuffers.insert(std::make_pair(name, std::unique_ptr<sf::SoundBuffer>(soundbuffer)));
}

sf::SoundBuffer& Resources::soundbuffer(std::string name)
{
  auto itr = m_soundbuffers.find(name);
  assert( "soundbuffer not found" && itr != m_soundbuffers.end() );
  return *m_soundbuffers[name].get();
}

void Resources::load_music(std::string name, std::string path)
{
  auto itr = m_musics.find(name);
  assert( itr == m_musics.end() );

  sf::Music* music = new sf::Music();
  music->openFromFile(path);

  m_musics.insert(std::make_pair(name, std::unique_ptr<sf::Music>(music)));
}

sf::Music& Resources::music(std::string name)
{
  auto itr = m_musics.find(name);
  assert( itr != m_musics.end() );
  return *m_musics[name].get();

}

Resources::~Resources()
{
  
}
