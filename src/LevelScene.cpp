/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelScene.hpp>
#include <LevelLoader.hpp>
#include <cassert>

LevelScene::LevelScene(Core& core)
  : m_core (core)
{
  m_player = std::make_unique<Player>(core);
  m_core.physic().add(*m_player);

    
  m_camera = std::make_unique<Camera>(m_core, *m_player);

  // Load level  
  LevelLoader ll(m_core, m_core.data_dir() + "levels/test.tmx");

  // monsters
  {
    std::vector<std::unique_ptr<Tile>> monsters;  
    load_level<Tile>(ll, monsters, "spawners");

    for (auto& m_tile : monsters)
      {
	std::unique_ptr<Monster> m = std::make_unique<Monster>(m_core,
							       m_tile->pos());
	m_core.physic().add(*m);
	m_monsters.push_back( std::move(m) );
      }
  }
  
  // player
  {
    std::vector<std::unique_ptr<Tile>> player;  
    load_level<Tile>(ll, player, "player");
    if ( !player.empty() )
      {
	m_player->pos( player[0]->pos() );
      }
  }
  
  load_level<Tile>(ll, m_bg_tiles, "background");
  load_level<Tile>(ll, m_platforms_tiles, "platforms");
  load_level<Tile>(ll, m_fragiles_tiles, "fragiles");
  load_level<GoalTile>(ll, m_goals_tiles, "goals");
  load_level<Tile>(ll, m_hurts_tiles, "hurts");
  
  for (auto& tile : m_platforms_tiles)
    {
      m_core.physic().add(*tile.get());
    }

  for (auto& tile : m_fragiles_tiles)
    {
      tile->fragile(true);
      m_core.physic().add(*tile.get());
    }

  for (auto& tile : m_goals_tiles)
    {
      tile->ghost(true);
      m_core.physic().add(*tile.get());
    }

  for (auto& tile : m_hurts_tiles)
    {
      tile->hurt(true);
      m_core.physic().add(*tile.get());
    }

  create_tileset(m_bg_tiles, m_bg_vertices);
  create_tileset(m_platforms_tiles, m_platforms_vertices);
}

void LevelScene::create_tileset(std::vector<std::unique_ptr<Tile>>& tiles, sf::VertexArray& array)
{
  array.setPrimitiveType(sf::Quads);
  array.resize(tiles.size()*4);
  
  int i = 0;
  
  for (auto& tile : tiles)
    {      
      array[i].position = {
	tile->center().x - tile->size().x/2,
	tile->center().y - tile->size().y/2
      };
      array[i+1].position = {
	tile->center().x + tile->size().x/2,
	tile->center().y - tile->size().y/2
      };
      array[i+2].position = {
	tile->center().x + tile->size().x/2,
	tile->center().y + tile->size().y/2
      };
      array[i+3].position = {
	tile->center().x - tile->size().x/2,
	tile->center().y + tile->size().y/2	
      };
      
      array[i].texCoords = {
      	(float)(tile->shape().getTextureRect().left),
	(float)(tile->shape().getTextureRect().top)
      };      
      array[i+1].texCoords = {
      	(float)(tile->shape().getTextureRect().left
		+ tile->shape().getTextureRect().width),
      	(float)(tile->shape().getTextureRect().top)
      };                  
      array[i+2].texCoords = {
      	(float)(tile->shape().getTextureRect().left
		+ tile->shape().getTextureRect().width),
      	(float)(tile->shape().getTextureRect().top
		+ tile->shape().getTextureRect().height)
      };      	
      array[i+3].texCoords = {
      	(float)tile->shape().getTextureRect().left,
      	(float)(tile->shape().getTextureRect().top
		+ tile->shape().getTextureRect().height)
      };
      
      i += 4;
    }
}

/*virtual*/ void LevelScene::update(float dt)
{
  for (auto& tile : m_bg_tiles)
    {
      tile->update(dt);
    }
  
  for (auto& tile : m_platforms_tiles)
    {
      if ( m_core.visible( tile->rect() ) )
	{
	  tile->update(dt);
	}
    }
  
  for (auto& tile : m_fragiles_tiles)
    {
      tile->update(dt);
    }

  for (auto& tile : m_goals_tiles)
    {
      if ( m_core.visible( tile->rect() ) )
	{
	  tile->update(dt);
	}      
    }

  for (auto& tile : m_hurts_tiles)
    {
      if ( m_core.visible( tile->rect() ) )
	{
	  tile->update(dt);
	}      
    }
  
  for (auto& monster : m_monsters)
    {
      monster->update(dt);
    }
  
  m_player->update(dt);
  m_camera->update(dt);
  m_core.physic().update(dt);
}

/*virtual*/ void LevelScene::display()
{  
  m_core.window().draw( m_bg_vertices,
			&m_core.resources().texture("texture:tileset"));

  m_core.window().draw( m_platforms_vertices,
			&m_core.resources().texture("texture:tileset"));
  
  for (auto& tile : m_fragiles_tiles)
    {
      if ( m_core.visible( tile->rect() ) )
  	{
  	  tile->display();
  	}
    }

  for (auto& tile : m_goals_tiles)
    {
      if ( m_core.visible( tile->rect() ) )
  	{
  	  tile->display();
  	}
    }

  for (auto& tile : m_hurts_tiles)
    {
      if ( m_core.visible( tile->rect() ) )
  	{
  	  tile->display();
  	}
    }
  
  for (auto& monster : m_monsters)
    {
      if ( m_core.visible( monster->rect() ) )
  	{
  	  monster->display();
  	}
    }
  
  m_player->display();
  m_camera->display();
 }

LevelScene::~LevelScene()
{
  
}
