/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RIGIDBODY_HPP
#define RIGIDBODY_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

enum class BodyID {
  Player,
  Monster,
  Tile,
  Wall,
  Goal,
  None
};

class RigidBody
{
public:
  RigidBody()
    : m_grounded(false)
    , m_fixed (true)
    , m_id (BodyID::None)
    , m_fragile (false)
    , m_ghost (false)
    , m_hurt (false)
  {}
  
  virtual void apply_force(sf::Vector2f force) {};
  virtual void collide_with(RigidBody& body, sf::Vector2f normal) {}
  
  virtual void move(sf::Vector2f depl)
  {
    m_pos.x += depl.x;
    m_pos.y += depl.y;
  }

  virtual sf::Vector2f pos() const { return m_pos; }
  virtual void pos(sf::Vector2f pos) { m_pos = pos; }
  
  virtual sf::Vector2f center() const
  {
    return {
      m_pos.x + m_size.x/2,
	m_pos.y + m_size.y/2
    };
  }
  
  virtual sf::Vector2f size() const { return m_size; }
  virtual sf::FloatRect rect () const
  {
    return {m_pos.x, m_pos.y, m_size.x, m_size.y};
  }
  
  virtual sf::Vector2f vel() const { return m_vel; }
  virtual void vel(sf::Vector2f vel) { m_vel = vel; }

  virtual sf::Vector2f old_vel() const { return m_old_vel; }
  virtual void old_vel(sf::Vector2f old_vel) { m_old_vel = old_vel; }
  
  virtual sf::Vector2f speed() const { return m_speed; }
  
  virtual bool grounded() const { return m_grounded; }
  virtual void grounded(bool grounded) { m_grounded = grounded; }

  virtual bool fixed() const { return m_fixed; }
  virtual void fixed(bool fixed) { m_fixed = fixed; }

  virtual BodyID id () { return m_id; }
  virtual void id (BodyID id) { m_id = id; }

  virtual void phy_update(float dt) {};

  virtual bool fragile () const { return m_fragile; }
  virtual void fragile (bool fragile) { m_fragile = fragile; }

  virtual bool ghost () const { return m_ghost; }
  virtual void ghost (bool ghost) { m_ghost = ghost; }

  virtual bool falling () const { return m_falling; }
  virtual void falling (bool falling) { m_falling = falling; }

  virtual void detach ()
  {
    ghost(true);
    fixed(false);
  }

  virtual bool hurt () const { return m_hurt; }
  virtual void hurt (bool hurt) { m_hurt = hurt; }
  
  virtual ~RigidBody() {}
  
protected:
  sf::Vector2f m_pos;
  sf::Vector2f m_vel;
  sf::Vector2f m_old_vel;
  sf::Vector2f m_size;
  sf::Vector2f m_speed;
  bool m_grounded;
  bool m_fixed;
  BodyID m_id;
  bool m_fragile;
  bool m_ghost;
  bool m_falling;
  bool m_hurt;
  
private:
  RigidBody( RigidBody const& rigidbody ) = delete;
  RigidBody& operator=( RigidBody const& rigidbody ) = delete;
};

#endif
