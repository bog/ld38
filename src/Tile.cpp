/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Tile.hpp>
#include <Core.hpp>

Tile::Tile(Core& core, sf::FloatRect rect)
  : m_core (core)
{
  m_id = BodyID::Tile;
  m_vel = {0.0f, 0.0f};
  
  m_pos = {
    rect.left,
    rect.top
  };

  m_size = {
    rect.width,
    rect.height
  };
}

/*virtual*/ void Tile::update(float dt)
{
  m_shape.setPosition(m_pos);
  m_shape.setSize(m_size);
}

/*virtual*/ void Tile::phy_update(float dt)
{
  m_pos.x += m_vel.x * dt;
  m_pos.y += m_vel.y * dt;
  m_vel = {0.0f, 0.0f};
}

/*virtual*/ void Tile::apply_force(sf::Vector2f force)
{
  m_vel.x += force.x;
  m_vel.y += force.y;
}

/*virtual*/ void Tile::detach()
{
  RigidBody::detach();
}

/*virtual*/ void Tile::display()
{
  m_core.window().draw(m_shape);
}

Tile::~Tile()
{
  
}
