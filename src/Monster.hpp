/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MONSTER_HPP
#define MONSTER_HPP
#include <iostream>
#include <Entity.hpp>
#include <RigidBody.hpp>

class Monster : public Entity, public RigidBody
{
public:
  Monster(Core& core, sf::Vector2f pos);
  virtual void update(float dt) override;
  virtual void phy_update(float dt) override;
  virtual void apply_force(sf::Vector2f force) override;
  virtual void collide_with(RigidBody& body, sf::Vector2f normal) override;
  virtual void display() override;
  virtual ~Monster();
  
protected:
  Core& m_core;
  sf::RectangleShape m_shape;
  float m_x_dir;
private:
  Monster( Monster const& monster ) = delete;
  Monster& operator=( Monster const& monster ) = delete;
};

#endif
