/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RESOURCES_HPP
#define RESOURCES_HPP
#include <iostream>
#include <memory>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Resources
{
public:
  explicit Resources();
  void load_font(std::string name, std::string path);
  sf::Font& font(std::string name);

  void load_texture(std::string name, std::string path);
  sf::Texture& texture(std::string name);

  void load_soundbuffer(std::string name, std::string path);
  sf::SoundBuffer& soundbuffer(std::string name);

  void load_music(std::string name, std::string path);
  sf::Music& music(std::string name);
  
  virtual ~Resources();
  
protected:
  std::unordered_map<std::string, std::unique_ptr<sf::Font>> m_fonts;
  std::unordered_map<std::string, std::unique_ptr<sf::Texture>> m_textures;
  std::unordered_map<std::string, std::unique_ptr<sf::SoundBuffer>> m_soundbuffers;
  std::unordered_map<std::string, std::unique_ptr<sf::Music>> m_musics;
  
private:
  Resources( Resources const& resources ) = delete;
  Resources& operator=( Resources const& resources ) = delete;
};

#endif
