#ifndef TRANSITIONSCENE_HPP
#define TRANSITIONSCENE_HPP
#include <iostream>
#include <functional>
#include <Entity.hpp>
#include <Core.hpp>

class TransitionScene : public Entity
{
public:
  TransitionScene(Core& core, std::string bg_name);
  virtual bool key(sf::Keyboard::Key key);
  virtual void reset();
  virtual void update(float dt) override;
  virtual void display() override;
  virtual ~TransitionScene();
    
protected:
  Core& m_core;
  sf::RectangleShape m_bg;
  sf::Clock m_clock;
  float m_input_delay;
private:
  TransitionScene( TransitionScene const& transitionscene ) = delete;
  TransitionScene& operator=( TransitionScene const& transitionscene ) = delete;
};

struct MenuScene : public TransitionScene
{
  MenuScene(Core& core, std::string name)
    : TransitionScene(core, name)
  {}
  
  virtual void update(float dt) override
  {
    if (key(sf::Keyboard::Escape))
      {
	m_core.quit();
	reset();
      }

    if (key(sf::Keyboard::H))
      {
	m_core.go_help();
	reset();
      }
    
    if (key(sf::Keyboard::Space))
      {
	m_core.go_play();
	reset();
      }
  }
};

struct WinScene : public TransitionScene
{
  WinScene(Core& core, std::string name)
    : TransitionScene(core, name)
  {}
  
  virtual void update(float dt) override
  {
    if (key(sf::Keyboard::Escape))
      {
	m_core.quit();
	reset();
      }
    
    if (key(sf::Keyboard::Space))
      {
	m_core.go_menu();
	reset();
      }
  }
};

struct FailScene : public TransitionScene
{
  FailScene(Core& core, std::string name)
    : TransitionScene(core, name)
  {}
  
  virtual void update(float dt) override
  {   
    if (key(sf::Keyboard::Escape))
      {
	m_core.quit();
	reset();
      }
    
    if (key(sf::Keyboard::Space))
      {
	m_core.go_menu();
	reset();
      }
  }
};

struct HelpScene : public TransitionScene
{
  HelpScene(Core& core, std::string name)
    : TransitionScene(core, name)
  {}
  
  virtual void update(float dt) override
  {   
    if (key(sf::Keyboard::Escape))
      {
	m_core.quit();
	reset();
      }
    
    if (key(sf::Keyboard::Space))
      {
	m_core.go_menu();
	reset();
      }
  }
};

#endif
