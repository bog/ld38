/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVELSCENE_HPP
#define LEVELSCENE_HPP
#include <iostream>
#include <Entity.hpp>
#include <Player.hpp>
#include <LevelLoader.hpp>
#include <GoalTile.hpp>
#include <Core.hpp>
#include <Camera.hpp>
#include <Monster.hpp>

class LevelScene : public Entity
{
public:
  LevelScene(Core& core);
  void create_tileset(std::vector<std::unique_ptr<Tile>>& tiles, sf::VertexArray& array);
  template<typename TileType>
  void load_level(LevelLoader& ll, std::vector<std::unique_ptr<TileType>>& tiles, std::string layer);
  virtual void update(float dt) override;
  virtual void display() override;  
  virtual ~LevelScene();
  
protected:
  Core& m_core;
  std::unique_ptr<Player> m_player;
  std::vector<std::unique_ptr<Monster>> m_monsters;
  std::unique_ptr<Camera> m_camera;
  std::vector<std::unique_ptr<Tile>> m_bg_tiles;
  std::vector<std::unique_ptr<Tile>> m_platforms_tiles;
  std::vector<std::unique_ptr<Tile>> m_fragiles_tiles;
  std::vector<std::unique_ptr<GoalTile>> m_goals_tiles;
  std::vector<std::unique_ptr<Tile>> m_hurts_tiles;
  sf::VertexArray m_bg_vertices;
  sf::VertexArray m_platforms_vertices;
  
private:
  LevelScene( LevelScene const& levelscene ) = delete;
  LevelScene& operator=( LevelScene const& levelscene ) = delete;
};

template<typename TileType>
void LevelScene::load_level(LevelLoader& ll, std::vector<std::unique_ptr<TileType>>& tiles, std::string layer)
{
  auto& vec = ll.layer(layer);
  int i = 0;
  
  for (sf::Vector2f const& v : vec)
    {
      if (v.x == -1 && v.y == -1)
	{
	  i++;
	  continue;
	}

      sf::FloatRect rect = {
	ll.tile_width() * (i%ll.width()),
	ll.tile_height() * (i/ll.width()),
	ll.tile_width(),
        ll.tile_height()
      };
      
      std::unique_ptr<TileType> tile = std::make_unique<TileType>(m_core, rect);
      tile->shape().setTexture(&m_core.resources().texture("texture:tileset"));
      tile->shape().setTextureRect({
	  static_cast<int>(v.x * ll.tile_width()),
	    static_cast<int>(v.y * ll.tile_height()),
	    static_cast<int>( ll.tile_width() ),
	    static_cast<int>( ll.tile_height() )
	    });

      tiles.push_back( std::move(tile) );
      i++;
    }
}

#endif
