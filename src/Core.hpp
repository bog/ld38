/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Entity.hpp>
#include <Resources.hpp>
#include <Vector.hpp>
#include <Physic.hpp>
#define DT_RATIO 8.0

class TransitionScene;

class Core : public Entity
{
public:
  Core(sf::RenderWindow& window);
  void load();
  virtual void update(float dt);
  virtual void display();
  void run();
  virtual ~Core();
  
  /* Services */
  sf::RenderWindow& window();
  sf::Vector2f window_size();
  sf::Vector2f world_size();
  sf::Vector2f gravity();
  Resources& resources();
  Physic& physic();
  std::string data_dir();
  bool visible(sf::FloatRect rect);
  void go_play();
  void go_menu();
  void go_win();
  void go_fail();
  void go_help();
  void quit();
  void music(std::string name="", float volume = 100.0);
  
protected:
  sf::RenderWindow& m_window;
  std::unique_ptr<Resources> m_resources;
  std::unique_ptr<Physic> m_physic;
  std::unique_ptr<Entity> m_level_scene;
  std::unique_ptr<TransitionScene> m_menu_scene;
  std::unique_ptr<TransitionScene> m_win_scene;
  std::unique_ptr<TransitionScene> m_fail_scene;
  std::unique_ptr<TransitionScene> m_help_scene;
  sf::Music* m_music;
  
  Entity* m_current_scene;
  
private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
