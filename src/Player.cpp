/* Copyright (C) 2017 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Player.hpp>
#include <Core.hpp>
#include <cassert>

Player::Player(Core& core)
  : m_core (core)
{
  m_size = {32, 64};
    
  m_pos = {
    0,
    m_core.world_size().y - m_size.y
  };
  
  m_life = 3;
  m_id = BodyID::Player;
  m_fixed = false;

  m_speed = {256, 256};
  m_jump_height = 0.0;
  m_jumping = false;
  m_fall = 0.0;
  m_fall_limit = 0.7;
  
  m_hurted = false;
  m_hurt_force = {0.0f, 0.0f};
  m_hurt_timer = 0.0f;
  m_hurt_time = 0.1f;
  m_shape.setFillColor({32, 192, 32});

  m_jump_sound.setBuffer(m_core.resources().soundbuffer("soundbuffer:jump"));
  m_hit_sound.setBuffer(m_core.resources().soundbuffer("soundbuffer:hit"));
  m_land_sound.setBuffer(m_core.resources().soundbuffer("soundbuffer:land"));
  m_land_sound_played = true;
  m_die_sound.setBuffer(m_core.resources().soundbuffer("soundbuffer:die"));
}

/*virtual*/ void Player::update(float dt)
{  
  m_shape.setPosition(m_pos);
  m_shape.setSize(m_size);
}

/*virtual*/ void Player::phy_update(float dt)
{
  if (m_life > 0)
    {
      update_inputs(dt);
      update_jump(dt);

      if (m_hurted)
	{
	  apply_force(m_hurt_force);
	  m_hurt_timer += dt;
      
	  if (m_hurt_timer >= m_hurt_time)
	    {
	      m_shape.setFillColor({100, 200, 100});
	      m_hurt_force = {0.0f, 0.0f};
	      m_hurted = false;
	      m_hurt_timer = 0.0f;	      
	    }
	}
    }

  m_pos.x += m_vel.x * dt;
  m_pos.y += m_vel.y * dt;
  m_old_vel = m_vel;
  m_vel = {0.0, 0.0};

  // fall
  if (!m_falling)
    {
      if (m_fall >= m_fall_limit*2)
  	{
	  hurt(m_life);
  	}
      else if (m_fall >= m_fall_limit)
  	{
  	  hurt();
  	}
      
      m_fall = 0.0f;
    }
  else
    {
      m_fall += dt;
    }

}

/*virtual*/ void Player::apply_force(sf::Vector2f force)
{    
  m_vel.x += force.x;
  m_vel.y += force.y;
}

/*virtual*/ void Player::collide_with(RigidBody& body, sf::Vector2f normal)
{
  if (m_life <= 0) { return; }

  if ( body.hurt() ) { hurt(); }
  
  if (body.id() == BodyID::Goal)
    {
      m_core.go_win();
    }
  
  if (body.id() == BodyID::Monster && !m_hurted)
    {
      hurt();
      m_hurt_force = normal;
      m_hurt_force.x *= m_speed.x;
      m_hurt_force.y *= m_speed.y - normal.y * m_core.gravity().y;      
    }

  // landing
  if ((body.id() == BodyID::Tile
      || body.id() == BodyID::Wall)
      && normal.y == -1)
    {
      if (!m_land_sound_played)
	{
	  m_land_sound_played = true;
	  m_land_sound.play();
	}
    }
}

/*virtual*/ void Player::display()
{
  m_core.window().draw(m_shape);
}

Player::~Player()
{  
}

void Player::update_inputs(float dt)
{
  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Left) )
    {      
      m_vel.x -= m_speed.x;
    }

  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Right) )
    {
      m_vel.x += m_speed.x;
    }

  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Space) )
    {
      jump();
    }
}

void Player::jump()
{
  if (m_grounded)
    {      
      m_land_sound_played = false;
      m_jump_sound.play();
      m_jump_height = m_core.gravity().y*2;
      m_jumping = true;
      m_grounded = false;
    }
}

void Player::update_jump(float dt)
{
  if (!m_jumping) { return; }
  
  apply_force({0.0f, -m_jump_height});

  m_jump_height -= m_core.gravity().y*2 * dt;
  if ( m_jump_height < 0.0f )
    {
      m_jump_height = 0.0f;
      m_jumping = false;
    }
}

void Player::hurt(int damages)
{
  m_hit_sound.play();
  m_hurted = true;      
  m_shape.setFillColor({200, 0, 0});

  m_life -= damages;
	      
  if (m_life <= 0)
    {
      m_life = 0;
      m_shape.setFillColor({100, 100, 100});
      m_die_sound.play();
      m_core.go_fail();
    }
}
