/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef VECTOR_HPP
#define VECTOR_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Vector
{
public:
  static std::string str (sf::Vector2f const& vec)
  {
    return "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ")";
  }

  static std::string str (sf::FloatRect const& rect)
  {
    return "(" +
      std::to_string(rect.left) +
      ", " +
      std::to_string(rect.top) +
      ", " +
      std::to_string(rect.width) +
      ", " +
      std::to_string(rect.height) +
      ")";
  }
  
protected:
  
private:
  Vector( Vector const& vector ) = delete;
  Vector& operator=( Vector const& vector ) = delete;
};

#endif
