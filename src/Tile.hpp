/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TILE_HPP
#define TILE_HPP
#include <iostream>
#include <Entity.hpp>
#include <RigidBody.hpp>

class Tile : public Entity, public RigidBody
{
public:
  Tile(Core& core, sf::FloatRect rect);
  virtual sf::RectangleShape& shape() { return m_shape; }
  virtual void update(float dt) override;
  virtual void phy_update(float dt) override;
  virtual void apply_force(sf::Vector2f force) override;
  virtual void detach() override;
  virtual void display() override;
  virtual ~Tile();
  
protected:
  Core& m_core;
  sf::RectangleShape m_shape;
 
private:
  Tile( Tile const& tile ) = delete;
  Tile& operator=( Tile const& tile ) = delete;
};

#endif
