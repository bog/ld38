/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <TransitionScene.hpp>
#include <Core.hpp>

TransitionScene::TransitionScene(Core& core, std::string bg_name)
  : m_core (core)
  , m_input_delay (0.3f)
{
  m_bg.setSize(m_core.window_size());
  m_bg.setTexture(&m_core.resources().texture(bg_name));
}

/*virtual*/ bool TransitionScene::key(sf::Keyboard::Key key)
{
  return ((m_clock.getElapsedTime().asSeconds() > m_input_delay)
	  && (sf::Keyboard::isKeyPressed(key)));
}

/*virtual*/ void TransitionScene::reset()
{
  m_clock.restart();
  sf::View view({0, 0, m_core.window_size().x, m_core.window_size().y});
  m_core.window().setView(view);
}

/*virtual*/ void TransitionScene::update(float dt)
{
  
}

/*virtual*/ void TransitionScene::display()
{
  m_core.window().draw(m_bg);
}

TransitionScene::~TransitionScene()
{
  
}
