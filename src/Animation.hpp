/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ANIMATION_HPP
#define ANIMATION_HPP
#include <iostream>
#include <unordered_map>
#include <Entity.hpp>

struct Frame
{
  int x;
  int y;
  float duration;
};

class Animation : public Entity
{
public:
  Animation(sf::RectangleShape& shape, sf::Texture& texture, sf::Vector2f tile_size);
  void add_frame(int animation, Frame frame);
  virtual void update(float dt) override;
  virtual ~Animation();

  int animation() { return m_current_animation; }
  void animation(int animation);

protected:
  sf::RectangleShape& m_shape;
  sf::Texture& m_texture;
  sf::Vector2f m_tile_size;
  std::unordered_map<int, std::vector<Frame>> m_animations;
  size_t m_current_frame;
  float m_timer;
  int m_current_animation;

  std::vector<Frame>& frames ();
private:
  Animation( Animation const& animation ) = delete;
  Animation& operator=( Animation const& animation ) = delete;
};

#endif
