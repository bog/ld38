/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Core.hpp>
#include <LevelScene.hpp>
#include <TransitionScene.hpp>

Core::Core(sf::RenderWindow& window)
  : m_window (window)
{
  m_resources = std::make_unique<Resources>();
  load();
  m_physic = std::make_unique<Physic>(*this);
  
  m_current_scene = nullptr;
  m_level_scene = std::make_unique<LevelScene>(*this);
  m_menu_scene = std::make_unique<MenuScene>(*this, "texture:main_scene");
  m_win_scene = std::make_unique<WinScene>(*this, "texture:win_scene");
  m_fail_scene = std::make_unique<FailScene>(*this, "texture:fail_scene");
  m_help_scene = std::make_unique<HelpScene>(*this, "texture:help_scene");
  
  m_current_scene = m_menu_scene.get();
  m_music = nullptr;
  music("music:ascension");
}

void Core::load()
{
  m_resources->load_texture("texture:tileset", data_dir() + "sprites/tileset.png");
  m_resources->load_texture("texture:player_spritesheet",
			    data_dir() + "sprites/player_spritesheet.png");
  m_resources->load_texture("texture:main_scene", data_dir() + "backgrounds/main.png");
  m_resources->load_texture("texture:win_scene", data_dir() + "backgrounds/win.png");
  m_resources->load_texture("texture:fail_scene", data_dir() + "backgrounds/fail.png");
  m_resources->load_texture("texture:help_scene", data_dir() + "backgrounds/help.png");

  m_resources->load_soundbuffer("soundbuffer:jump", data_dir() + "sounds/jump.ogg");
  m_resources->load_soundbuffer("soundbuffer:hit", data_dir() + "sounds/hit.ogg");
  m_resources->load_soundbuffer("soundbuffer:land", data_dir() + "sounds/land.ogg");
  m_resources->load_soundbuffer("soundbuffer:die", data_dir() + "sounds/die.ogg");
  m_resources->load_soundbuffer("soundbuffer:fragile", data_dir() + "sounds/fragile.ogg");

  m_resources->load_music("music:look_up", data_dir() + "musics/look_up.ogg");
  m_resources->load_music("music:reached", data_dir() + "musics/reached.ogg");
  m_resources->load_music("music:fall_down", data_dir() + "musics/fall_down.ogg");
  m_resources->load_music("music:ascension", data_dir() + "musics/ascension.ogg");
  m_resources->load_music("music:the_sky_limit", data_dir() + "musics/the_sky_limit.ogg");
}

/*virtual*/ void Core::update(float dt)
{
  if (m_current_scene)
    {
      m_current_scene->update(dt);
    }
}

/*virtual*/ void Core::display()
{
  if (m_current_scene)
    {
      m_current_scene->display();
    }
}

void Core::run()
{
  sf::Event event;
  
  float dt = 0.0f;
  sf::Clock clock;
  
  while ( m_window.isOpen() )
    {
      while ( m_window.pollEvent(event) )
	{
	  switch (event.type)
	    {
	    case sf::Event::Closed :
	      m_window.close();
	      break;
	      
	    case sf::Event::KeyPressed :
	      switch (event.key.code)
		{
		case sf::Keyboard::Escape :
		  if ( m_current_scene == m_level_scene.get() )
		    {
		      go_menu();
		    }
		  break;
		  
		default : break;
		}
	      break;

	    default : break;
	    }
	}

      update(dt*DT_RATIO);
      
      m_window.clear();
      display();
      m_window.display();
      
      dt = clock.getElapsedTime().asMicroseconds()/1000000.0;
      clock.restart();     
    }  
}

Core::~Core()
{
  
}

sf::RenderWindow& Core::window()
{
  return m_window;
}

sf::Vector2f Core::window_size()
{
  return {
    static_cast<float>(m_window.getSize().x),
      static_cast<float>(m_window.getSize().y)
      };
	   
}

sf::Vector2f Core::world_size()
{
  return { 32*80, 32*160 };
}

sf::Vector2f Core::gravity()
{
  return {0, 512};
}

Resources& Core::resources()
{
  return *m_resources.get();
}

Physic& Core::physic()
{
  return *m_physic.get();
}

std::string Core::data_dir()
{
  return "../assets/";
}

bool Core::visible(sf::FloatRect rect)
{
  sf::View view = m_window.getView();
  float merge = 64.0;
  return !(rect.left + merge < view.getCenter().x - view.getSize().x/2
	   || rect.left + rect.width - merge > view.getCenter().x + view.getSize().x/2
	   || rect.top + merge < view.getCenter().y - view.getSize().y/2
	   || rect.top + rect.height - merge > view.getCenter().y + view.getSize().y/2);
 
}

void Core::go_play()
{
  m_physic.reset(new Physic(*this));
  m_level_scene.reset(new LevelScene(*this));
  m_current_scene = m_level_scene.get();
  music("music:the_sky_limit", 25.0);
}

void Core::go_menu()
{
  m_current_scene = m_menu_scene.get();
  m_menu_scene->reset();
  music("music:ascension");
}

void Core::go_win()
{
  m_current_scene = m_win_scene.get();
  m_win_scene->reset();
  music("music:reached");
}

void Core::go_fail()
{
  m_current_scene = m_fail_scene.get();
  m_fail_scene->reset();
  music("music:fall_down");
  m_music->setLoop(false);
}

void Core::quit()
{
  m_window.close();
}

void Core::go_help()
{
  m_current_scene = m_help_scene.get();
  m_help_scene->reset();
  music("music:look_up");  
}

void Core::music(std::string name, float volume)
{
  if (m_music != nullptr)
    {
      m_music->stop();
    }

  if ( !name.empty() )
    {
      m_music = &m_resources->music(name);
      m_music->setVolume(volume);
      m_music->setLoop(true);
      m_music->play();
    }
}
