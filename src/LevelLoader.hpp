/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVELLOADER_HPP
#define LEVELLOADER_HPP
#include <iostream>
#include <tinyxml2.h>
#include <unordered_map>
#include <vector>
#include <SFML/Graphics.hpp>

namespace tx = tinyxml2;

class Core;

class LevelLoader
{
public:
  LevelLoader(Core& core, std::string path);
  void process_layer(tx::XMLElement* layer);
  std::vector<sf::Vector2f> const& layer(std::string name);

  unsigned int width () const { return m_width; }
  unsigned int height () const { return m_height; }

  float tile_width () const { return m_tile_width; }
  float tile_height () const { return m_tile_height; }
  
  virtual ~LevelLoader();
protected:
  Core& m_core;
  std::unordered_map<std::string, std::vector<sf::Vector2f>> m_map;
  float m_tile_width;
  float m_tile_height;
  unsigned int m_tileset_width;
  unsigned int m_tileset_height;
  unsigned int m_width;
  unsigned int m_height;
  
private:
  LevelLoader( LevelLoader const& levelloader ) = delete;
  LevelLoader& operator=( LevelLoader const& levelloader ) = delete;
};

#endif
