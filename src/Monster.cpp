/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Monster.hpp>
#include <Core.hpp>

Monster::Monster(Core& core, sf::Vector2f pos)
  : m_core (core)
  , m_x_dir (1)
{
  m_id = BodyID::Monster;
  m_fixed = false;
  m_pos = pos;
  m_size = {32, 64};
  m_vel = {0.0f, 0.0f};
  m_speed = {64, 0};
}

/*virtual*/ void Monster::update(float dt)
{
  m_shape.setPosition(m_pos);
  m_shape.setSize(m_size);
  m_shape.setFillColor({192, 32, 32});    
}

/*virtual*/ void Monster::phy_update(float dt)
{ 
  if (m_grounded)
    {
      apply_force({m_x_dir * m_speed.x, 0});
    }
  
  m_pos.x += m_vel.x * dt;
  m_pos.y += m_vel.y * dt;
  m_old_vel = m_vel;
  m_vel = {0.0f, 0.0f};
}

/*virtual*/ void Monster::apply_force(sf::Vector2f force)
{
  m_vel.x += force.x;
  m_vel.y += force.y;
}

/*virtual*/ void Monster::collide_with(RigidBody& body, sf::Vector2f normal)
{
  if (normal.x != 0)
    {
      m_x_dir *= -1;
    }  
}

/*virtual*/ void Monster::display()
{
  m_core.window().draw(m_shape);
}

Monster::~Monster()
{
  
}
