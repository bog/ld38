/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Animation.hpp>
#include <cassert>
#include <Core.hpp>

Animation::Animation(sf::RectangleShape& shape, sf::Texture& texture, sf::Vector2f tile_size)
  : m_shape (shape)
  , m_texture (texture)
  , m_tile_size (tile_size)
  , m_current_frame (0)
  , m_timer (0.0f)
  , m_current_animation (0)
{
  
}

void Animation::add_frame(int animation, Frame frame)
{
  if ( m_animations.find(animation) == m_animations.end() )
    {
      m_animations[animation] = std::vector<Frame>();
    }
  
  m_animations[animation].push_back(frame);
}

/*virtual*/ void Animation::update(float dt)
{
  if (frames().empty()) { return; }
  
  m_timer += dt/DT_RATIO;

  if (m_timer >= frames()[m_current_frame].duration)
    {
      m_current_frame++;
      
      if ( m_current_frame >= frames().size() )
	{
	  m_current_frame = 0;
	}
      
      m_shape.setTexture(&m_texture);
      m_shape.setTextureRect({
	  static_cast<int>(frames()[m_current_frame].x * m_tile_size.x),
	    static_cast<int>(frames()[m_current_frame].y * m_tile_size.y),
	    static_cast<int>(m_tile_size.x),
	    static_cast<int>(m_tile_size.y)
	    });
      
      m_timer = 0.0f;
    }
}

Animation::~Animation()
{
  
}

void Animation::animation(int animation)
{
  assert("animation not found"
	 && (m_animations.find(animation) != m_animations.end()));

  if (m_current_animation != animation)
    {
      m_current_frame = 0;
      m_current_animation = animation;
    }
}

std::vector<Frame>& Animation::frames ()
{
  return m_animations[m_current_animation];
}
