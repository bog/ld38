/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CAMERA_HPP
#define CAMERA_HPP
#include <iostream>
#include <Entity.hpp>

class Player;

class Camera : public Entity
{
public:
  Camera(Core& core, Player& player);
  virtual void update(float dt) override;
  virtual ~Camera();
  
protected:
  Core& m_core;
  Player& m_player;
  
private:
  Camera( Camera const& camera ) = delete;
  Camera& operator=( Camera const& camera ) = delete;
};

#endif
