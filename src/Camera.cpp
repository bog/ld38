/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Camera.hpp>
#include <Core.hpp>
#include <Player.hpp>

Camera::Camera(Core& core, Player& player)
  : m_core (core)
  , m_player (player)
{
  
}

/*virtual*/ void Camera::update(float dt)
{
  sf::View view = m_core.window().getView();

  view.setCenter( m_player.center() );
  
  // Collision with screen
  if (view.getCenter().x - view.getSize().x/2 < 0)
    {
      view.setCenter({view.getSize().x/2, view.getCenter().y});
    }
  if (view.getCenter().x + view.getSize().x/2 > m_core.world_size().x)
    {
      view.setCenter({m_core.world_size().x - view.getSize().x/2, view.getCenter().y});
    }
  if (view.getCenter().y - view.getSize().y/2 < 0)
    {
      view.setCenter({view.getCenter().x, view.getSize().y/2});
    }
  if (view.getCenter().y + view.getSize().y/2 > m_core.world_size().y)
    {
      view.setCenter({view.getCenter().x, m_core.world_size().y - view.getSize().y/2});
    }
  
  m_core.window().setView(view);
}

Camera::~Camera()
{
  
}
