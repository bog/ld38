/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelLoader.hpp>
#include <Core.hpp>
#include <cassert>

LevelLoader::LevelLoader(Core& core, std::string path)
  : m_core (core)
{
  tx::XMLDocument doc;
  tx::XMLError err = doc.LoadFile( path.c_str() );
  assert(err == 0);
  
  tx::XMLNode* node = doc.FirstChild()->NextSibling();
  
  // Map
  tx::XMLElement* map = static_cast<tx::XMLElement*>(node);
  m_tile_width = static_cast<float>( atoi( map->Attribute("tilewidth") ) );
  m_tile_height = static_cast<float>( atoi( map->Attribute("tileheight") ) );
  m_width = static_cast<unsigned int>( atoi( map->Attribute("width") ) );
  m_height = static_cast<unsigned int>( atoi( map->Attribute("height") ) );

  // Tileset
  node = node->FirstChildElement("tileset");
  m_tileset_width = static_cast<unsigned int>( atoi( map->Attribute("tilewidth") ) );
  m_tileset_height = static_cast<unsigned int>( atoi( map->Attribute("tileheight") ) );

  // Layers
  node = node->NextSiblingElement("layer");
  
  while (node != nullptr)
    {
      process_layer( static_cast<tx::XMLElement*>(node) );
      node = node->NextSiblingElement("layer");
    }
}

void LevelLoader::process_layer(tx::XMLElement* layer)
{
  std::string name( layer->Attribute("name") );
  m_map[name] = std::vector<sf::Vector2f>();
  
  tx::XMLElement* tile = layer->FirstChildElement()->FirstChildElement();
  
  while ( tile != nullptr )
    {
      unsigned int id = atoi( tile->Attribute("gid") );
      
      if (id == 0)
	{
	  m_map[name].push_back({-1, -1});
	}
      else
	{
	  id--;
      
	  sf::Vector2f pos;
	  
	  pos.x = id % m_tileset_width;
	  pos.y = id/m_tileset_width;

	  m_map[name].push_back(pos);
	}
      
      tile = tile->NextSiblingElement();
    }
}

std::vector<sf::Vector2f> const& LevelLoader::layer(std::string name)
{
  assert("layer not found" && m_map.find(name) != m_map.end() );
  return m_map[name];
}

LevelLoader::~LevelLoader()
{
  
}
