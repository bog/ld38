/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <SFML/Graphics.hpp>
#include <Core.hpp>

int main ()
{
  sf::VideoMode mode = sf::VideoMode::getDesktopMode();
  
  mode.width *= 0.8;
  mode.height *= 0.8;
  
  sf::RenderWindow window(mode, "A small world", sf::Style::Titlebar | sf::Style::Close);
  window.setFramerateLimit(60.0);
  
  Core core(window);
  core.run();

  return 0;
}
