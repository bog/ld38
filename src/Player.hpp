/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include <RigidBody.hpp>
#include <Entity.hpp>

class Player : public RigidBody, public Entity
{
public:
  Player(Core& core);
  virtual void update(float dt) override;
  virtual void phy_update(float dt) override;
  virtual void apply_force(sf::Vector2f force) override;
  virtual void collide_with(RigidBody& body, sf::Vector2f normal) override;
  virtual void display() override;  
  virtual ~Player();
  
protected:
  sf::RectangleShape m_shape;
  Core& m_core;
  float m_jump_height;
  bool m_jumping;
  bool m_hurted;
  sf::Vector2f m_hurt_force;
  float m_hurt_timer;
  float m_hurt_time;
  int m_life;
  sf::Sound m_jump_sound;
  sf::Sound m_hit_sound;
  sf::Sound m_land_sound;
  bool m_land_sound_played;
  sf::Sound m_die_sound;
  
  float m_fall;
  float m_fall_limit;
  
  void update_inputs(float dt);
  void jump();
  void update_jump(float dt);
  void hurt(int damages=1);
private:
  Player( Player const& player ) = delete;
  Player& operator=( Player const& player ) = delete;
};

#endif
