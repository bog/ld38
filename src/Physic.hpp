/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PHYSIC_HPP
#define PHYSIC_HPP
#include <iostream>
#include <vector>
#include <Entity.hpp>

class RigidBody;

class Physic : public Entity
{
public:
  Physic(Core& core);
  void add(RigidBody& body);
  void remove(RigidBody& body);
  virtual void update(float dt) override;
  virtual ~Physic();
  
protected:
  Core& m_core;
  std::vector<RigidBody*> m_bodies;
  float m_acc;
  float m_period;
  sf::Sound m_detach_sound;
  
  void update_bodies(float dt);
  void update_gravity(RigidBody& rb, float dt);
  void update_screen_collision(RigidBody& rb, float dt);
  void update_bodies_collision(RigidBody& rb1, RigidBody& rb2, float dt);
  
private:
  Physic( Physic const& physic ) = delete;
  Physic& operator=( Physic const& physic ) = delete;
};

#endif
