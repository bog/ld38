/* Copyright (C) 2017 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GOALTILE_HPP
#define GOALTILE_HPP
#include <iostream>
#include <Tile.hpp>

class GoalTile : public Tile
{
public:
  GoalTile(Core& core, sf::FloatRect rect);
  virtual ~GoalTile();
  
protected:
  
private:
  GoalTile( GoalTile const& goaltile ) = delete;
  GoalTile& operator=( GoalTile const& goaltile ) = delete;
};

#endif
