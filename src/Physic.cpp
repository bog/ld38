/* Copyright (C) 2017 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Physic.hpp>
#include <RigidBody.hpp>
#include <Core.hpp>
#include <cmath>

Physic::Physic(Core& core)
  : m_core (core)
{
  m_period = 1/60.0f;
  m_acc = 0.0f;
  m_detach_sound.setBuffer(m_core.resources().soundbuffer("soundbuffer:fragile"));
}

void Physic::add(RigidBody& body)
{
  m_bodies.push_back(&body);
}

void Physic::remove(RigidBody& body)
{
  m_bodies.erase(std::remove_if(m_bodies.begin(), m_bodies.end(), [&](RigidBody* rb){
	return rb == & body;
      }), m_bodies.end());
}

/*virtual*/ void Physic::update(float dt)
{
  m_acc += dt; 
  
  if (m_acc > m_period)
    {
      update_bodies(m_period);
      m_acc = 0;
    }
}

Physic::~Physic()
{
}

void Physic::update_bodies(float dt)
{
  for (size_t i=0; i<m_bodies.size(); i++)
    {
      RigidBody& rb1 = *m_bodies[i];
      
      if ( !m_core.visible(rb1.rect()) ) { continue; }
      
      rb1.phy_update(dt);

      rb1.falling(rb1.old_vel().y > 0);
      
      if ( !rb1.fixed() )
	{	  	  
	  for (size_t j=i+1; j<m_bodies.size(); j++)
	    {
	      RigidBody& rb2 = *m_bodies[j];
	      if ( !m_core.visible(rb2.rect()) ) { continue; }
	      update_bodies_collision(rb1, rb2, dt);
	    }
	  update_gravity(rb1, dt);
	  update_screen_collision(rb1, dt);	  
	}
    }
}

void Physic::update_gravity(RigidBody& rb, float dt)
{
  if ( !rb.fixed() )
    {
      rb.apply_force( m_core.gravity() );
    }
}

void Physic::update_screen_collision(RigidBody& rb, float dt)
{
  sf::Vector2f world = m_core.world_size();
  RigidBody wall;
  wall.id(BodyID::Wall);
  
  if (rb.pos().x < 0)
    {
      rb.move({-rb.pos().x, 0.0f});
      rb.collide_with(wall, {1, 0});
    }

  if (rb.pos().x + rb.size().x > world.x)
    {
      rb.move({world.x - (rb.pos().x + rb.size().x), 0.0f});
      rb.collide_with(wall, {-1, 0});
    }

  if (rb.pos().y < 0)
    {
      rb.move({0.0f, -rb.pos().y});
      rb.collide_with(wall, {0, 1});
    }

  if (rb.pos().y + rb.size().y > world.y)
    {
      rb.move({0.0f, world.y - (rb.pos().y + rb.size().y)});
      rb.collide_with(wall, {0, -1});
      rb.grounded(true);
      rb.falling(false);
    }
}

void Physic::update_bodies_collision(RigidBody& rb1, RigidBody& rb2, float dt)
{
  if (rb1.id() == BodyID::Monster
      && rb2.id() == BodyID::Monster) { return; }
  
  sf::FloatRect intersection;
  if ( !rb1.rect().intersects(rb2.rect(), intersection) ) { return; }
  
  float x_overlap =
    abs(rb1.center().x - rb2.center().x)
    - rb1.size().x/2
    - rb2.size().x/2;
		     
  float y_overlap =
    abs(rb1.center().y - rb2.center().y)
    - rb1.size().y/2
    - rb2.size().y/2;
  
  sf::Vector2f normal = {0.0f, 0.0f};
  
  if ( abs(x_overlap) < abs(y_overlap) 
       && rb2.center().y < rb1.center().y + rb1.size().y/2 )
    {
      if (rb1.center().x < rb2.center().x)
	{
	  normal = {-1, 0};
	}
      else
	{
	  normal = {1, 0};
	}
    }
  else
    {
      if (rb1.center().y < rb2.center().y)
	{
	  normal = {0, -1};
	}
      else
	{
	  normal = {0, 1};
	}
    }
  
  if ( !rb1.fixed() || rb1.ghost() )
    {
      sf::Vector2f v = rb1.old_vel();
      float vv = sqrt(v.x*v.x + v.y*v.y);
      v.x /= vv;
      v.y /= vv;

      sf::Vector2f m = {
	normal.x * intersection.width,
	normal.y * intersection.height
      };
      float mm = sqrt(m.x*m.x + m.y*m.y);
      m.x /= mm;
      m.y /= mm;
      
      float dot = m.x * v.x + m.y * v.y;
      
      if ( dot <= 0 )
	{
	  if ( !rb1.ghost() && !rb2.ghost() )
	    {
	      rb1.move({
		  normal.x * intersection.width,
		    normal.y * intersection.height
		    });
	    }
	}
      else
	{
	  return;
	}

      if (normal.x == 0 && normal.y == -1)
	{
	  rb1.grounded(true);
	}
      
      rb1.collide_with(rb2, normal);
      
      if (normal.y == -1)
	{
	  rb1.falling(false);
	}
	  
      if ( rb1.id() != BodyID::Monster && rb2.fragile() && !rb2.ghost() )
	{
	  rb2.detach();
	  m_detach_sound.play();
	}
    }
  
  if ( !rb2.fixed() || rb2.ghost() )
    {
      sf::Vector2f v = rb2.old_vel();
      float vv = sqrt(v.x*v.x + v.y*v.y);
      v.x /= vv;
      v.y /= vv;

      sf::Vector2f m = {
	normal.x * intersection.width,
	normal.y * intersection.height
      };
      float mm = sqrt(m.x*m.x + m.y*m.y);
      m.x /= mm;
      m.y /= mm;
      
      float dot = m.x * v.x + m.y * v.y;
      
      if ( dot <= 0 )
	{
	  if ( !rb1.ghost() && !rb2.ghost() )
	    {
	      rb2.move({
		  -normal.x * intersection.width,
		    -normal.y * intersection.height
		    });
	    }
	}
      else
	{
	  return;
	}

      if (normal.x == 0 && normal.y == 1)
	{
	  rb2.grounded(true);
	}
      
      rb2.collide_with(rb1, {-normal.x, -normal.y});

      if ( rb2.id() != BodyID::Monster && rb1.fragile() && !rb1.ghost() )
	{
	  rb1.detach();
	  m_detach_sound.play();
	}
    }
}
