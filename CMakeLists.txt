cmake_minimum_required(VERSION 3.5)

project(ld38)

include_directories(src)

file(GLOB
  sources
  src/*.cpp
  )

if(MINGW)
  set(SFML_DIR "lib/SFML-2.4.1")
  include_directories("${SFML_DIR}/include" "lib/tinyxml2/include")  
  link_directories("${SFML_DIR}/lib" "lib/tinyxml2/lib")
endif(MINGW)

add_definitions("-Wall -g -std=c++14")

add_executable(ld38.elf ${sources})

find_package(PkgConfig)
pkg_check_modules(SFML sfml-all)

target_link_libraries(ld38.elf
  ${SFML_LDFLAGS}
  )